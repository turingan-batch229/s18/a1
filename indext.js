/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function checkSum(num){
	let num1 = 5;
	let num2 = 15
	console.log("Displayed sum of " + num1 + " and " + num2);

	let isSum = (num1 + num2);
	
	console.log(isSum);
}


checkSum();

function checkDiff(num){
	let num3 = 20;
	let num4 = 5;
	console.log("Displayed difference of " + num3 + " and " + num4);

	let isDiff = (num3 - num4);
	
	console.log(isDiff);
}


checkDiff();


function checkProd(num){
	let num5 = 50;
	let num6 = 10;
	console.log("The product of " + num5 + " and " + num6);

	let isProd = (num5 * num6);
	
	console.log(isProd);
}


checkProd();


function checkQuot(num){
	let num7 = 50;
	let num8 = 10;
	console.log("The quotient of " + num7 + " and " + num8);

	let isQuot = (num7 / num8);
	
	console.log(isQuot);
}


checkQuot();

function checkQuot(num){
	let num7 = 50;
	let num8 = 10;
	console.log("The quotient of " + num7 + " and " + num8);

	let isQuot = (num7 / num8);
	
	console.log(isQuot);
}


checkQuot();

function checkArea(num){
	let pi = 3.1416;
	let num9 = 15;
	console.log("The quotient of getting the area of a circle with a " + num9 + " radius.");

	let isArea = (pi * (num9 * num9));
	
	console.log(isArea);
}


checkArea();


function checkAvg(num){
	let num10 = 20;
	let num11 = 40;
	let num12 = 60;
	let num13 = 80;
	console.log("The average of " + num10 + ", " +  num11 + ", " +  num12 + " and " + num13 + " :");

	let isAvg = ((num10 + num11 + num12 + num13)/ 4);
	
	console.log(isAvg);
}


checkAvg();


function checkPassing(num){
	let pass = 50;
	let score = 38;

	console.log("Is " + score + "/" + pass + " a passing score?");

	let total = ((score / pass) * 100);
	let total2 = 76;

	let isPassingBy50 = total === total2;
		console.log(isPassingBy50);
}


checkPassing();
